
import { React, useState, useEffect } from 'react';

function GuestCheckout() {
  return (
    <>
      {/*thông tin đăng kí*/}
      <div className="col-xs-12 col-sm-6" style={{borderLeft: '2px solid', paddingTop: '60px'}}>
        <p style={{fontSize: '25px', textAlign: 'center', borderBottom: '1px solid'}}>GUESTS</p>
        <p>-100% Satisfaction guaranteed (28 day return service)</p>
        <p>- Return by courier pick up, or in store</p>
        <p>- For orders above $80/ $95 Standard Shipping is always FREE!</p>
        <br />
        <p style={{fontWeight: 700}}>Join FootGear team and you will get</p>
        <p>- The latest and greatest best gear for you</p>
        <p>- Expert guidance, whatever your goals are</p>
        <p>- Access to workouts and incredible events</p>
        <div className="form-group">
          <div className="col-sm-1 col-md-1 col-lg-1 col-xs-1" />
          <div className="col-sm-11 col-md-11 col-lg-11 col-xs-10" style={{textAlign: 'center'}}>
            <button id="valuser" type="button" onclick="submitForm()" className="btn btn-success btn-guess-checkout">
              GUESTS CHECKOUT
            </button>
          </div>
          <div className="col-sm-1 col-md-1 col-lg-1 col-xs-1" />
        </div>
      </div>
      {/*kết thúc  form đăng kí*/}
    </>
  );
}
export default GuestCheckout;