import { React, useState, useEffect } from 'react';

function SignUp() {
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [password_confirmation, setPassword_confirmation] = useState("");

  const [emailErr, setEmailErr] = useState(false);
  const [usernameErr, setUsernameErr] = useState(false);
  const [passwordErr, setPasswordErr] = useState(false);
  const [password_confirmationErr, setPassword_confirmationErr] = useState(false);

  const checkValidDate = () => {
    let valid = true;
    const validateEmail = new RegExp("^[a-z0-9]+@gmail([\.])com$");
    const validatePassword = new RegExp(
      "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*[!@$^%()])[A-Za-][a-zA-Z0-9!@$%^&()]{8,}$"
    );
    const validateUsername = new RegExp("^[a-zA-Z0-9]{1,20}$");
    if (!email || !username || !password || !password_confirmation) {
      if (!email) setEmailErr(true);
      if (!username) setUsernameErr(true);
      if (!password) setPasswordErr(true);
      if (!password_confirmation) setPassword_confirmationErr(true);
      valid = false;
    }
    if (email && validateEmail.test(email)) {
      setEmailErr(false);
      valid = true;
    }
    if (email && !validateEmail.test(email)) {
      setEmailErr(true);
      valid = false;
    }
    if (username && validateUsername.test(username)) {
      setUsernameErr(false);
      valid = true;
    }
    if (username && !validateUsername.test(username)) {
      setUsernameErr(true);
      valid = false;
    }
    if (password && validatePassword.test(password)) {
      setPasswordErr(false);
      valid = true;
    }
    if (password && !validatePassword.test(password)) {
      setPasswordErr(true);
      valid = false;
    }
    if (password_confirmation && password_confirmation === password) {
      setPassword_confirmationErr(false);
      valid = true;
    }
    if (password_confirmation && password_confirmation !== password) {
      setPassword_confirmationErr(true);
      valid = false;
    }
    return valid;
  };

  const clickRegister = () => {
    let data = {
      email: email, username: username,
      password: password, password_confirmation: password_confirmation,
    };
    let valid = checkValidDate();
    console.log(valid), 782;
    if (valid){
      console.log(data, 895);
    }
  };

  return (
    <>
      <div className="col-xs-12 col-sm-6" style={{borderLeft: '2px solid', paddingTop: '60px'}}>
        <p style={{fontSize: '25px'}}>REGISTER NEW ACCOUNT</p>
        <div style={{marginLeft: '-80px'}} className="form-horizontal">
          <div className="form-group" style={{marginBottom: '0px'}}>
            <div className="col-sm-4 col-md-4 col-lg-5 col-xs-1" />
            <div className="col-sm-8 col-md-8 col-lg-7 col-xs-10 mobilePad" id="message10" style={{fontSize: '10pt', paddingLeft: '0px'}} />
          </div>
          <div className="form-group">
            <div className="col-sm-1 col-md-1 col-lg-1 col-xs-1" />
            <div className="col-sm-3 col-md-3 col-lg-4 col-xs-10 mobileLabel" style={{paddingTop: '7px', textAlign: 'right'}}>
              Username <span style={{color: 'red'}}>*</span> :
            </div>
            <div className="col-sm-7 col-md-7 col-lg-6 col-xs-9 input-group mobilePad" style={{fontWeight: 600}}>
              <input style={{borderRadius: '4px!important'}} type="text" className="form-control" name="username" 
                value={username} onChange={(e) => setUsername(e.target.value)} id="username" />
            </div>
            <div className="col-sm-1 col-md-1 col-lg-1 col-xs-1" />
          </div>
          <div className="form-group">
            <div className="col-sm-1 col-md-1 col-lg-1 col-xs-1" />
            <div className="col-sm-3 col-md-3 col-lg-4 col-xs-10 mobileLabel" style={{paddingTop: '7px', textAlign: 'right'}}>
              Your Email <span style={{color: 'red'}}>*</span> :
            </div>
            <div className="col-sm-7 col-md-7 col-lg-6 col-xs-9 input-group mobilePad" style={{fontWeight: 600}}>
              <input style={{borderRadius: '4px!important'}} type="email" className="form-control" name="yourEmail" 
                value={email} onChange={(e) => setEmail(e.target.value)} id="yourEmail" />
            </div>
            <div className="col-sm-1 col-md-1 col-lg-1 col-xs-1" />
          </div>
          <div className="form-group " style={{marginBottom: '5px'}}>
            <div className="col-sm-1 col-md-1 col-lg-1 col-xs-1" />
            <div className="col-sm-3 col-md-3 col-lg-4 col-xs-10 mobileLabel" style={{paddingTop: '7px', textAlign: 'right'}}>
              Your Password <span style={{color: 'red'}}>*</span> :
            </div>
            <div className="col-sm-7 col-md-7 col-lg-6 col-xs-9 input-group mobilePad">
              <input type="password" /*onkeyup="passwordChecker()"*/ name="password" id="yourPassword" className="form-control" 
                value={password} onChange={(e) => setPassword(e.target.value)} />
              <span className="input-group-btn">
                <button className="btn btn-defaultCUST" id="view_button3" style={{height: '34px', paddingLeft: '7px'}} type="button">
                  <span className="glyphicon glyphicon-eye-open" />
                </button>
              </span>
            </div>
            <div className="col-sm-1 col-md-1 col-lg-1 col-xs-1" />
          </div>
          {/* <div className="form-group" style={{marginBottom: '5px'}}>
            <div className="col-sm-4 col-md-4 col-lg-5 col-xs-1" />
            <div className="col-sm-8 col-md-8 col-lg-7 col-xs-10 mobilePad" id="message8" style={{fontSize: '10pt', paddingLeft: '0px'}} />
            <div className="col-sm-4 col-md-4 col-lg-5 col-xs-1" />
            <div className="col-sm-8 col-md-8 col-lg-7 col-xs-10 mobilePad" id="message" style={{fontSize: '10pt'}} />
            <div className="col-sm-4 col-md-4 col-lg-5 col-xs-1" />
            <div className="col-sm-8 col-md-8 col-lg-7 col-xs-10 mobilePad" id="message2" style={{fontSize: '10pt'}} />
            <div className="col-sm-4 col-md-4 col-lg-5 col-xs-1" />
            <div className="col-sm-8 col-md-8 col-lg-7 col-xs-10 mobilePad" id="message3" style={{fontSize: '10pt'}} />
            <div className="col-sm-4 col-md-4 col-lg-5 col-xs-1" />
            <div className="col-sm-8 col-md-8 col-lg-7 col-xs-10 mobilePad" id="message4" style={{fontSize: '10pt'}} />
            <div className="col-sm-4 col-md-4 col-lg-5 col-xs-1" />
            <div className="col-sm-8 col-md-8 col-lg-7 col-xs-10 mobilePad" id="message5" style={{fontSize: '10pt'}} />
            <div className="col-sm-4 col-md-4 col-lg-5 col-xs-1" />
            <div className="col-sm-8 col-md-8 col-lg-7 col-xs-10 mobilePad" id="message6" style={{fontSize: '10pt', paddingLeft: '0px'}} />
            <div className="col-sm-4 col-md-4 col-lg-5 col-xs-1" />
            <div className="col-sm-8 col-md-8 col-lg-7 col-xs-10 mobilePad" id="message7" style={{fontSize: '10pt', paddingLeft: '0px'}} />
          </div> */}
          <div className="form-group">
            <div className="col-sm-1 col-md-1 col-lg-1 col-xs-1" />
            <div className="col-sm-3 col-md-3 col-lg-4 col-xs-10 mobileLabel" style={{paddingTop: '7px', textAlign: 'right'}}>
              Confirm Your Password <span style={{color: 'red'}}>*</span> :
            </div>
            <div className="col-sm-7 col-md-7 col-lg-6 col-xs-9 input-group mobilePad">
              <input type="password" name="verifypassword" id="verifypassword" className="form-control" 
                value={password_confirmation} onChange={(e) => setPassword_confirmation(e.target.value)} />
                <span className="input-group-btn">
                  <button className="btn btn-defaultCUST" id="view_button4" style={{height: '34px', paddingLeft: '7px'}} type="button">
                    <span className="glyphicon glyphicon-eye-open" />
                </button>
              </span>
            </div>
            <div className="col-sm-1 col-md-1 col-lg-1 col-xs-1" />
          </div>
          <div className="form-group">
            <div className="col-sm-12 col-md-12 col-lg-12 col-xs-12" id="message1" style={{fontWeight: 'bold', textAlign: 'center', fontSize: '10pt'}}>
            </div>
          </div>
          <div className="form-group">
            <div className="col-sm-1 col-md-1 col-lg-1 col-xs-1" />
            <div className="col-sm-11 col-md-11 col-lg-11 col-xs-10" style={{textAlign: 'center'}}>
              <label style={{fontSize: '15px', fontWeight: 500}}><input type="checkbox" defaultValue /*unchecked*/ />&nbsp;I have read and agree to the Terms &amp; Conditions for website use
              </label>
              <button id="valuser" type="submit" onClick={() => clickRegister()} className="btn btn-success">
                REGISTER NEW ACCOUNT
              </button>
            </div>
            <div className="col-sm-1 col-md-1 col-lg-1 col-xs-1" />
          </div>
        </div>
      </div>
    </>
  );
}
export default SignUp;