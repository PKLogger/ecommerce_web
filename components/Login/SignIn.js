
import { React, useState, useEffect } from 'react';
import { Modal, Button } from "react-bootstrap";
import { useForm } from "react-hook-form";

function SignIn() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [emailError, setEmailError] = useState(false);
  const [passwordError, setPasswordError] = useState(false);
  const [showSucessSubmit, setShowSucessSubmit] = useState(false);
  const { register, setFocus } = useForm();
  const handleCloseSucessSubmit = () => {
    setShowSucessSubmit(false);
    window.location.reload();
  };
  const handleShowSucessSubmit = () => setShowSucessSubmit(true);

  const checkValidDate = () => {
    let valid = true;
    if (!email || !password ) {
      if (!email) {
        setFocus("email");
        setEmailError(true);
      }
      if (!password) {
        setFocus("password");
        setPasswordError(true);
      }
      valid = false;
    }
    if (email) {
      setEmailError(false);
      valid = true;
    }
    if (password) {
      setPasswordError(false);
      valid = true;
    }
    return valid;
  };

  const handleSignIn = (e) => {
    e.preventDefault();
    let data = {
      email: email,
      password: password,
    };
    console.log("🚀 ~ file: SignIn.js ~ line 15 ~ handleSignIn ~ data", data)
    let valid = checkValidDate();
    if (valid) {
      handleShowSucessSubmit();
    }
  };
  return (
    <>
      <div className="col-xs-12 col-sm-6" style={{paddingTop: '60px'}}>
        <p style={{fontSize: '25px'}}>LOGIN IN</p>
        <div className="c-login-form">
          <div className="form-group">
            <label htmlFor="email">Email Adress: <span style={{color: 'red'}}>*</span></label>
            <input 
              type="email" className="form-control" id="email" placeholder="Enter email" 
              value={email} {...register("email")}
              onChange={(e) => setEmail(e.target.value)} style={{width: '315px', marginBottom: "5px"}}
            />
            {emailError ? <small>Email chưa có hoặc không đúng định dạng</small> : null}
          </div>
          <div className="form-group">
            <label htmlFor="password">Password: <span style={{color: 'red'}}>*</span></label>
            <input 
              type="password" className="form-control" id="password" placeholder="Enter password" 
              value={password} {...register("password")}
              onChange={(e) => setPassword(e.target.value)} style={{width: '315px', marginBottom: "5px"}}
            />
            {passwordError ? <small>Mật khẩu chưa có hoặc không đúng định dạng</small> : null}
          </div>
          <div className="form-group">
            <label style={{fontSize: '1.2em'}}>
              <input type="checkbox" defaultValue />&nbsp;Remmember Me
            </label>
          </div>
          <div className="form-group col-xs-12 col-sm-2 col-sm-offset-4">
            <button type="submit" className="btn btn-success" onClick={(e) => handleSignIn(e)} id="btn-click-login">SIGN IN</button>
          </div>
        </div>
      </div>
    </>
  );
}
export default SignIn;