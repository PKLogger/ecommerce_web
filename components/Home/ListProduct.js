import React from 'react';

function ListProduct(props) {
  return (
    <>
      {/*phần danh sách sản phẩm*/}
      <div className="list-product">
        <div className="container">
          <div className="row">
            <div className="col-sm-6 col-md-4" style={{cursor: 'pointer'}}>
              <div className="thumbnail">
                <img src="../code/images/anh1.jpg" alt="..." className="product" />
              </div>
            </div>
            <div className="col-sm-6 col-md-4" style={{cursor: 'pointer'}}>
              <div className="thumbnail">
                <img src="../code/images/anh2.jpg" alt="..." className="product" />
              </div>
            </div>
            <div className="col-sm-6 col-md-4" style={{cursor: 'pointer'}}>
              <div className="thumbnail">
                <img src="../code/images/anh3.jpg" alt="..." className="product" />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-6 col-md-4" style={{cursor: 'pointer'}}>
              <div className="thumbnail">
                <img src="../code/images/anh4.jpg" alt="..." className="product" />
              </div>
            </div>
            <div className="col-sm-6 col-md-4" style={{cursor: 'pointer'}}>
              <div className="thumbnail">
                <img src="../code/images/anh5.jpg" alt="..." className="product" />
              </div>
            </div>
            <div className="col-sm-6 col-md-4" style={{cursor: 'pointer'}}>
              <div className="thumbnail">
                <img src="../code/images/anh6.jpg" alt="..." className="product" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default ListProduct;