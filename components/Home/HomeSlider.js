import React from 'react';
import Slider from 'react-slick';

function HomeSlider(props) {
  const settingsHomeSlider = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    nextArrow: <SampleNextSquare />,
    prevArrow: <SamplePrevSquare />
  };
  
  function SampleNextSquare(props) {
    const { className, style, onClick } = props;
    return (
      <div className="slider-next-square" onClick={onClick}>
        <i className="fa fa-chevron-right" aria-hidden="true"></i>
      </div>
    );
  }
  
  function SamplePrevSquare(props) {
    const { className, style, onClick } = props;
    return (
      <div className="slider-prev-square" onClick={onClick}>
        <i className="fa fa-chevron-left" aria-hidden="true"></i>
      </div>
    );
  }

  return (
    <>
      <div className='home-slider'>
        <Slider {...settingsHomeSlider}>
          <div className="slider-item">
            <img src="images/banner1.jpg" alt="" data-action="zoom" />
          </div>
          <div className="slider-item">
            <img src="/images/banner2.jpg" alt="" data-action="zoom" />
          </div>
          <div className="slider-item">
            <img src="/images/banner3.jpg" alt="" data-action="zoom" />
          </div>
          <div className="slider-item">
            <img src="/images/banner1.jpg" alt="" data-action="zoom" />
          </div>
          <div className="slider-item">
            <img src="/images/banner2.jpg" alt="" data-action="zoom" />
          </div>
          <div className="slider-item">
            <img src="/images/banner3.jpg" alt="" data-action="zoom" />
          </div>
        </Slider>
      </div>
    </>
  );
}

export default HomeSlider;