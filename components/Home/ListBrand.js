import React from 'react';

function ListBrand(props) {
  return (
    <>
      <div className="list-brand">
        <div className="container">
          <div className="row">
            <div className="col-md-2 col-xs-2 nike"> <a href="#"><img src="images/logo/logo-acer.jpg" alt="br1" /></a></div>
            <div className="col-md-2 col-xs-2 jordan"> <a href="#"><img src="images/logo/logo-dell.jpg" alt="br2" /></a></div>
            <div className="col-md-2 col-xs-2 palladium"> <a href="#"><img src="images/logo/logo-intel.jpg" alt="br3" /></a></div>
            <div className="col-md-2 col-xs-2 adidas"> <a href="#"><img src="images/logo/logo-lenovo.png" alt="br4" /></a></div>
            <div className="col-md-2 col-xs-2 converse"> <a href="#"><img src="images/logo/logo-samsung.png" alt="br5" /></a></div>
            <div className="col-md-2 col-xs-2 van"> <a href="#"><img src="images/logo/logo-toshiba.png" alt="br6" /></a></div>
          </div>
        </div>
      </div>
    </>
  );
}

export default ListBrand;