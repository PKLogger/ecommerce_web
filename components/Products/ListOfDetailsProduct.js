
import { React, useState, useEffect } from 'react';

function ListOfDetailsProduct() {
  return (
    <>
      <div className="header-filter" style={{color: 'white', backgroundColor: '#121212', height: '40px', lineHeight: '40px', cursor: 'pointer', fontWeight: 'bold', textIndent: '2%'}}>
          LIST OF DETAILS PRODUCT
        </div>
        {/* phần hình ảnh panner quảng cáo 100%size */}
        <div className="img-panner" style={{marginBottom: '10px'}}>
          <img src="images/products/panner1.jpg" />
        </div>
        <div className="image-quangcao">
          <div className="row">
            <div className="col-xs-12 col-sm-4">
              <img src="images/products/producer1.jpg" />
            </div>
            <div className="col-xs-12 col-sm-4">
              <img src="images/products/producer2.jpg" />
            </div>
            <div className="col-xs-12 col-sm-4">
              <img src="images/products/producer3.jpg" />
            </div>
          </div>
        </div>
    </>
  );
}
export default ListOfDetailsProduct;