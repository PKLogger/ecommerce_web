
import { React, useState, useEffect } from 'react';

function FilterProduct() {
  return (
    <>
      <div className="header-filter" style={{color: 'white', backgroundColor: '#121212', height: '40px', lineHeight: '40px', cursor: 'pointer', fontWeight: 'bold', textIndent: '5%'}}>
        FILTER PRODUCTS
      </div>
      {/*loc san pham theo ten nha san xuat*/}
      <div className="filter-producer" style={{cursor: 'pointer'}}>
        <div className="header-producer" style={{height: '40px', lineHeight: '40px', clear: 'both', borderTop: '1px solid', borderBottom: '1px solid'}}>
          <p style={{float: 'left', fontWeight: 'bold', textIndent: '5%'}}>PRODUCER</p>
          <p style={{float: 'right'}}><span className="glyphicon glyphicon-chevron-down hide-show-producer" style={{fontSize: '20px'}} data-check="show" /></p>
        </div>
        <div className="body-producer">
          <ul className="list-group" style={{marginTop: '10px'}}>
            <li className="list-group-item" style={{padding: '0px'}}>
              <div className="checkbox">
                <label style={{fontSize: '1.2em'}}>
                  <input type="checkbox" defaultValue />
                  <span className="cr"><i className="cr-icon fa fa-check" /></span>
                  NIKE
                </label>
              </div>
            </li>
            <li className="list-group-item" style={{padding: '0px'}}>
              <div className="checkbox">
                <label style={{fontSize: '1.2em'}}>
                  <input type="checkbox" defaultValue />
                  <span className="cr"><i className="cr-icon fa fa-check" /></span>
                  VANS
                </label>
              </div>
            </li>
            <li className="list-group-item" style={{padding: '0px'}}>
              <div className="checkbox">
                <label style={{fontSize: '1.2em'}}>
                  <input type="checkbox" defaultValue />
                  <span className="cr"><i className="cr-icon fa fa-check" /></span>
                  KAPPA
                </label>
              </div>
            </li>
            <li className="list-group-item" style={{padding: '0px'}}>
              <div className="checkbox">
                <label style={{fontSize: '1.2em'}}>
                  <input type="checkbox" defaultValue defaultChecked />
                  <span className="cr"><i className="cr-icon fa fa-check" /></span>
                  ADIDAS
                </label>
              </div>
            </li>
            <li className="list-group-item" style={{padding: '0px'}}>
              <div className="checkbox">
                <label style={{fontSize: '1.2em'}}>
                  <input type="checkbox" defaultValue />
                  <span className="cr"><i className="cr-icon fa fa-check" /></span>
                  CONVERSE
                </label>
              </div>
            </li>
          </ul>
        </div>
      </div>
      {/*kết thúc lọc theo PRODUCER*/}
      {/*lọc theo model*/}
      <div className="filter-model" style={{cursor: 'pointer'}}>
        <div className="header-model" style={{height: '40px', lineHeight: '40px', clear: 'both', borderTop: '1px solid', borderBottom: '1px solid'}}>
          <p style={{float: 'left', fontWeight: 'bold', textIndent: '5%'}}>MODEL</p>
          <p style={{float: 'right'}}><span className="glyphicon glyphicon-chevron-down hide-show-model" style={{fontSize: '20px'}} data-check="show" /></p>
        </div>
        <div className="body-model">
          <ul className="list-group" style={{marginTop: '10px'}}>
            <li className="list-group-item" style={{padding: '0px'}}>
              <div className="checkbox">
                <label style={{fontSize: '1.2em'}}>
                  <input type="checkbox" defaultValue />
                  <span className="cr"><i className="cr-icon fa fa-check" /></span>
                  Old Skool
                </label>
              </div>
            </li>
            <li className="list-group-item" style={{padding: '0px'}}>
              <div className="checkbox">
                <label style={{fontSize: '1.2em'}}>
                  <input type="checkbox" defaultValue defaultChecked />
                  <span className="cr"><i className="cr-icon fa fa-check" /></span>
                  Califoria
                </label>
              </div>
            </li>
            <li className="list-group-item" style={{padding: '0px'}}>
              <div className="checkbox">
                <label style={{fontSize: '1.2em'}}>
                  <input type="checkbox" defaultValue />
                  <span className="cr"><i className="cr-icon fa fa-check" /></span>
                  Camo
                </label>
              </div>
            </li>
            <li className="list-group-item" style={{padding: '0px'}}>
              <div className="checkbox">
                <label style={{fontSize: '1.2em'}}>
                  <input type="checkbox" defaultValue />
                  <span className="cr"><i className="cr-icon fa fa-check" /></span>
                  Classic
                </label>
              </div>
            </li>
            <li className="list-group-item" style={{padding: '0px'}}>
              <div className="checkbox">
                <label style={{fontSize: '1.2em'}}>
                  <input type="checkbox" defaultValue />
                  <span className="cr"><i className="cr-icon fa fa-check" /></span>
                  Era
                </label>
              </div>
            </li>
            <li className="list-group-item" style={{padding: '0px'}}>
              <div className="checkbox">
                <label style={{fontSize: '1.2em'}}>
                  <input type="checkbox" defaultValue />
                  <span className="cr"><i className="cr-icon fa fa-check" /></span>
                  Iso
                </label>
              </div>
            </li>
            <li className="list-group-item" style={{padding: '0px'}}>
              <div className="checkbox">
                <label style={{fontSize: '1.2em'}}>
                  <input type="checkbox" defaultValue />
                  <span className="cr"><i className="cr-icon fa fa-check" /></span>
                  Off the wall
                </label>
              </div>
            </li>
          </ul>
        </div>
      </div>
      {/*kết thúc lọc theo model*/}
      {/*lọc theo SIZE*/}
      <div className="filter-size" style={{cursor: 'pointer'}}>
        <div className="header-size" style={{height: '40px', lineHeight: '40px', clear: 'both', borderTop: '1px solid', borderBottom: '1px solid'}}>
          <p style={{float: 'left', fontWeight: 'bold', textIndent: '5%'}}> SIZE</p>
          <p style={{float: 'right'}}><span className="glyphicon glyphicon-chevron-down hide-show-size" style={{fontSize: '20px'}} data-check="show" /></p>
        </div>
        <div className="body-size">
          <ul className="list-group" style={{marginTop: '10px'}}>
            <li className="list-group-item" style={{padding: '0px'}}>
              <div className="checkbox">
                <label style={{fontSize: '1.2em'}}>
                  <input type="checkbox" defaultValue />
                  <span className="cr"><i className="cr-icon fa fa-check" /></span>
                  XS
                </label>
              </div>
            </li>
            <li className="list-group-item" style={{padding: '0px'}}>
              <div className="checkbox">
                <label style={{fontSize: '1.2em'}}>
                  <input type="checkbox" defaultValue />
                  <span className="cr"><i className="cr-icon fa fa-check" /></span>
                  S
                </label>
              </div>
            </li>
            <li className="list-group-item" style={{padding: '0px'}}>
              <div className="checkbox">
                <label style={{fontSize: '1.2em'}}>
                  <input type="checkbox" defaultValue defaultChecked />
                  <span className="cr"><i className="cr-icon fa fa-check" /></span>
                  M
                </label>
              </div>
            </li>
            <li className="list-group-item" style={{padding: '0px'}}>
              <div className="checkbox">
                <label style={{fontSize: '1.2em'}}>
                  <input type="checkbox" defaultValue />
                  <span className="cr"><i className="cr-icon fa fa-check" /></span>
                  L
                </label>
              </div>
            </li>
            <li className="list-group-item" style={{padding: '0px'}}>
              <div className="checkbox">
                <label style={{fontSize: '1.2em'}}>
                  <input type="checkbox" defaultValue />
                  <span className="cr"><i className="cr-icon fa fa-check" /></span>
                  XL
                </label>
              </div>
            </li>
            <li className="list-group-item" style={{padding: '0px'}}>
              <div className="checkbox">
                <label style={{fontSize: '1.2em'}}>
                  <input type="checkbox" defaultValue />
                  <span className="cr"><i className="cr-icon fa fa-check" /></span>
                  XXL
                </label>
              </div>
            </li>
          </ul>
        </div>
      </div>
      {/*kết thúc lọc theo SIZE*/}
      <div className="filter-color" style={{cursor: 'pointer'}}>
        <div className="header-color" style={{height: '40px', lineHeight: '40px', clear: 'both', borderTop: '1px solid', borderBottom: '1px solid'}}>
          <p style={{float: 'left', fontWeight: 'bold', textIndent: '5%'}}>COLOR</p>
          <p style={{float: 'right'}}><span className="glyphicon glyphicon-chevron-down hide-show-color" style={{fontSize: '20px'}} data-check="show" /></p>
        </div>
        <div className="body-color">
          <ul className="list-group" style={{marginTop: '10px'}}>
            <li className="list-group-item" style={{padding: '0px'}}>
              <div className="checkbox">
                <label style={{fontSize: '1.2em'}}>
                  <input type="checkbox" defaultValue />
                  <span className="cr"><i className="cr-icon fa fa-check" /></span>
                  Black
                </label>
              </div>
            </li>
            <li className="list-group-item" style={{padding: '0px'}}>
              <div className="checkbox">
                <label style={{fontSize: '1.2em'}}>
                  <input type="checkbox" defaultValue />
                  <span className="cr"><i className="cr-icon fa fa-check" /></span>
                  Blue
                </label>
              </div>
            </li>
            <li className="list-group-item" style={{padding: '0px'}}>
              <div className="checkbox">
                <label style={{fontSize: '1.2em'}}>
                  <input type="checkbox" defaultValue defaultChecked />
                  <span className="cr"><i className="cr-icon fa fa-check" /></span>
                  Brown
                </label>
              </div>
            </li>
            <li className="list-group-item" style={{padding: '0px'}}>
              <div className="checkbox">
                <label style={{fontSize: '1.2em'}}>
                  <input type="checkbox" defaultValue />
                  <span className="cr"><i className="cr-icon fa fa-check" /></span>
                  Mutil
                </label>
              </div>
            </li>
            <li className="list-group-item" style={{padding: '0px'}}>
              <div className="checkbox">
                <label style={{fontSize: '1.2em'}}>
                  <input type="checkbox" defaultValue />
                  <span className="cr"><i className="cr-icon fa fa-check" /></span>
                  Yellow
                </label>
              </div>
            </li>
            <li className="list-group-item" style={{padding: '0px'}}>
              <div className="checkbox">
                <label style={{fontSize: '1.2em'}}>
                  <input type="checkbox" defaultValue />
                  <span className="cr"><i className="cr-icon fa fa-check" /></span>
                  White
                </label>
              </div>
            </li>
          </ul>
        </div>
      </div>
      {/*lọc theo màu sắc*/}
    </>
  );
}
export default FilterProduct;