
import { React, useState, useEffect } from 'react';

function JordanProduct() {
  return (
    <>
      {/*phần chính - danh sách các sản phẩm */}
      <div className="list-product" style={{marginTop: '10px'}}>
        <div className="col-xs-12" style={{padding: 0}}>
          <p style={{backgroundColor: '#121212', width: '100%', height: '40px', lineHeight: '40px', fontWeight: 'bold', color: 'white', marginBottom: '15px', textIndent: '2%'}}>
            JORDAN
          </p>
        </div>
        <div className="col-xs-12 col-sm-4 product" style={{padding: 0, height: '405px'}} id="pro3">
          <div className="sanpham">
            {/*phần đầu chứa hình ảnh của sản phẩm*/}
            <div className="header-image-product" style={{clear: 'both', height: '200px'}}>
              <div className="left" style={{width: '20%', height: '200px', float: 'left', paddingLeft: '10px'}}>
                <div className="box box1" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/sanpham/sanpham4.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
                <div className="box box2" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/sanpham/sanpham4.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
                <div className="box box3" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/sanpham/sanpham3.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
              </div>
              <div className="right" style={{width: '80%', float: 'left'}}>
                <img src="images/sanpham/sanpham3.jpg" className="img-responsive img-change" />
              </div>
            </div>
            {/*kết thúc phần đầu của sản phẩm*/}
            {/*phần thông tin của sản phẩm*/}
            <br />
            <br />
            <div className="info-product" align="center">
              <p className="name-product" style={{fontWeight: 'bold'}}>Converse Old Skool Platform -
                Women
                Shoes
              </p>
              <p className="price-product">$110</p>
              <p className="list-start">
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
              </p>
              {/*các nút yêu cầu mua hàng và View Product sản phẩm*/}
              <p className="footer-product">
                <button type="button" className="btn btn-success btn-click-muahang">Shop Now
                </button>
                <button type="button" className="btn btn-info btn-click-xem-chi-tiet">View Product
                </button>
              </p>
            </div>
            {/*kết thúc phần thông tin của sản phầm*/}
          </div>
        </div>
        <div className="col-xs-12 col-sm-4 product" style={{padding: 0, height: '385px'}} id="pro1">
          <div className="sanpham" id="sanpham1">
            {/*phần đầu chứa hình ảnh của sản phẩm*/}
            <div className="header-image-product" style={{clear: 'both', height: '200px'}}>
              <div className="left" style={{width: '20%', height: '200px', float: 'left', paddingLeft: '10px'}}>
                <div className="box box1" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/nike/nike1.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
                <div className="box box2" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/nike/nike2.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
                <div className="box box3" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/nike/nike3.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
              </div>
              <div className="right" style={{width: '80%', float: 'left'}}>
                <img src="images/nike/nike4.jpg" className="img-responsive img-change" />
              </div>
            </div>
            {/*kết thúc phần đầu của sản phẩm*/}
            {/*phần thông tin của sản phẩm*/}
            <br />
            <br />
            <div className="info-product" align="center">
              <p className="name-product" style={{fontWeight: 'bold'}}>Nike Old Skool Platform - Men
                Shoes
              </p>
              <p className="price-product">$110</p>
              <p className="list-start">
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
              </p>
              {/*các nút yêu cầu mua hàng và View Product sản phẩm*/}
              <p className="footer-product">
                <button type="button" className="btn btn-success btn-click-muahang">Shop Now
                </button>
                <button type="button" className="btn btn-info btn-click-xem-chi-tiet">View Product
                </button>
              </p>
            </div>
            {/*kết thúc phần thông tin của sản phầm*/}
          </div>
        </div>
        <div className="col-xs-12 col-sm-4 product" style={{padding: 0, height: '405px'}} id="pro3">
          <div className="sanpham">
            {/*phần đầu chứa hình ảnh của sản phẩm*/}
            <div className="header-image-product" style={{clear: 'both', height: '200px'}}>
              <div className="left" style={{width: '20%', height: '200px', float: 'left', paddingLeft: '10px'}}>
                <div className="box box1" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/puma/puma9.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
                <div className="box box2" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/puma/puma10.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
                <div className="box box3" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/puma/puma12.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
              </div>
              <div className="right" style={{width: '80%', float: 'left'}}>
                <img src="images/puma/puma13.jpg" className="img-responsive img-change" />
              </div>
            </div>
            {/*kết thúc phần đầu của sản phẩm*/}
            {/*phần thông tin của sản phẩm*/}
            <br />
            <br />
            <div className="info-product" align="center">
              <p className="name-product" style={{fontWeight: 'bold'}}>Converse Old Skool Platform - Women
                Shoes
              </p>
              <p className="price-product">$110</p>
              <p className="list-start">
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
              </p>
              {/*các nút yêu cầu mua hàng và View Product sản phẩm*/}
              <p className="footer-product">
                <button type="button" className="btn btn-success btn-click-muahang">Shop Now</button>
                <button type="button" className="btn btn-info btn-click-xem-chi-tiet">View Product
                </button>
              </p>
            </div>
            {/*kết thúc phần thông tin của sản phầm*/}
          </div>
        </div>
        <div className="col-xs-12 col-sm-4 product" style={{padding: 0, height: '385px'}} id="pro1">
          <div className="sanpham" id="sanpham1">
            {/*phần đầu chứa hình ảnh của sản phẩm*/}
            <div className="header-image-product" style={{clear: 'both', height: '200px'}}>
              <div className="left" style={{width: '20%', height: '200px', float: 'left', paddingLeft: '10px'}}>
                <div className="box box1" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/sanpham/sanpham2.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
                <div className="box box2" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/sanpham/sanpham4.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
                <div className="box box3" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/sanpham/sanpham3.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
              </div>
              <div className="right" style={{width: '80%', float: 'left'}}>
                <img src="images/sanpham/sanpham1.jpg" className="img-responsive img-change" />
              </div>
            </div>
            {/*kết thúc phần đầu của sản phẩm*/}
            {/*phần thông tin của sản phẩm*/}
            <br />
            <br />
            <div className="info-product" align="center">
              <p className="name-product" style={{fontWeight: 'bold'}}>Vans Old Skool Platform - Women
                Shoes
              </p>
              <p className="price-product">$110</p>
              <p className="list-start">
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
              </p>
              {/*các nút yêu cầu mua hàng và View Product sản phẩm*/}
              <p className="footer-product">
                <button type="button" className="btn btn-success btn-click-muahang">Shop Now</button>
                <button type="button" className="btn btn-info btn-click-xem-chi-tiet">View Product
                </button>
              </p>
            </div>
            {/*kết thúc phần thông tin của sản phầm*/}
          </div>
        </div>
        <div className="col-xs-12 col-sm-4 product" style={{padding: 0, height: '385px'}} id="pro2">
          <div className="sanpham">
            {/*phần đầu chứa hình ảnh của sản phẩm*/}
            <div className="header-image-product" style={{clear: 'both', height: '200px'}}>
              <div className="left" style={{width: '20%', height: '200px', float: 'left', paddingLeft: '10px'}}>
                <div className="box box1" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/sanpham/sanpham5.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
                <div className="box box2" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/sanpham/sanpham6.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
                <div className="box box3" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/sanpham/sanpham3.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
              </div>
              <div className="right" style={{width: '80%', float: 'left'}}>
                <img src="images/sanpham/sanpham2.jpg" className="img-responsive img-change" />
              </div>
            </div>
            {/*kết thúc phần đầu của sản phẩm*/}
            {/*phần thông tin của sản phẩm*/}
            <br />
            <br />
            <div className="info-product" align="center">
              <p className="name-product" style={{fontWeight: 'bold'}}>Vans Old Skool Platform - Women
                Shoes
              </p>
              <p className="price-product">$110</p>
              <p className="list-start">
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
              </p>
              {/*các nút yêu cầu mua hàng và View Product sản phẩm*/}
              <p className="footer-product">
                <button type="button" className="btn btn-success btn-click-muahang">Shop Now</button>
                <button type="button" className="btn btn-info btn-click-xem-chi-tiet">View Product
                </button>
              </p>
            </div>
            {/*kết thúc phần thông tin của sản phầm*/}
          </div>
        </div>
        <div className="col-xs-12 col-sm-4 product" style={{padding: 0, height: '385px'}} id="pro2">
          <div className="sanpham">
            {/*phần đầu chứa hình ảnh của sản phẩm*/}
            <div className="header-image-product" style={{clear: 'both', height: '200px'}}>
              <div className="left" style={{width: '20%', height: '200px', float: 'left', paddingLeft: '10px'}}>
                <div className="box box1" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/nike/nike5.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
                <div className="box box2" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/nike/nike7.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
                <div className="box box3" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/nike/nike8.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
              </div>
              <div className="right" style={{width: '80%', float: 'left'}}>
                <img src="images/nike/nike9.jpg" className="img-responsive img-change" />
              </div>
            </div>
            {/*kết thúc phần đầu của sản phẩm*/}
            {/*phần thông tin của sản phẩm*/}
            <br />
            <br />
            <div className="info-product" align="center">
              <p className="name-product" style={{fontWeight: 'bold'}}>Nike Old Skool Platform - Men
                Shoes
              </p>
              <p className="price-product">$110</p>
              <p className="list-start">
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
              </p>
              {/*các nút yêu cầu mua hàng và View Product sản phẩm*/}
              <p className="footer-product">
                <button type="button" className="btn btn-success btn-click-muahang">Shop Now
                </button>
                <button type="button" className="btn btn-info btn-click-xem-chi-tiet">View Product
                </button>
              </p>
            </div>
            {/*kết thúc phần thông tin của sản phầm*/}
          </div>
        </div>
        <div className="col-xs-12 col-sm-4 product" style={{padding: 0, height: '385px'}} id="pro3">
          <div className="sanpham">
            {/*phần đầu chứa hình ảnh của sản phẩm*/}
            <div className="header-image-product" style={{clear: 'both', height: '200px'}}>
              <div className="left" style={{width: '20%', height: '200px', float: 'left', paddingLeft: '10px'}}>
                <div className="box box1" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/nike/nike11.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
                <div className="box box2" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/nike/nike12.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
                <div className="box box3" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/nike/nike13.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
              </div>
              <div className="right" style={{width: '80%', float: 'left'}}>
                <img src="images/nike/nike14.jpg" className="img-responsive img-change" />
              </div>
            </div>
            {/*kết thúc phần đầu của sản phẩm*/}
            {/*phần thông tin của sản phẩm*/}
            <br />
            <br />
            <div className="info-product" align="center">
              <p className="name-product" style={{fontWeight: 'bold'}}>Nike Old Skool Platform - Kid
                Shoes
              </p>
              <p className="price-product">$110</p>
              <p className="list-start">
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
              </p>
              {/*các nút yêu cầu mua hàng và View Product sản phẩm*/}
              <p className="footer-product">
                <button type="button" className="btn btn-success btn-click-muahang">Shop Now
                </button>
                <button type="button" className="btn btn-info btn-click-xem-chi-tiet">View Product
                </button>
              </p>
            </div>
            {/*kết thúc phần thông tin của sản phầm*/}
          </div>
        </div>
        <div className="col-xs-12 col-sm-4 product" style={{padding: 0, height: '405px'}} id="pro1">
          <div className="sanpham" id="sanpham1">
            {/*phần đầu chứa hình ảnh của sản phẩm*/}
            <div className="header-image-product" style={{clear: 'both', height: '200px'}}>
              <div className="left" style={{width: '20%', height: '200px', float: 'left', paddingLeft: '10px'}}>
                <div className="box box1" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/puma/puma1.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
                <div className="box box2" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/puma/puma2.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
                <div className="box box3" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/puma/puma4.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
              </div>
              <div className="right" style={{width: '80%', float: 'left'}}>
                <img src="images/puma/puma5.jpg" className="img-responsive img-change" />
              </div>
            </div>
            {/*kết thúc phần đầu của sản phẩm*/}
            {/*phần thông tin của sản phẩm*/}
            <br />
            <br />
            <div className="info-product" align="center">
              <p className="name-product" style={{fontWeight: 'bold'}}>Puma Old Skool Platform - Women
                Shoes
              </p>
              <p className="price-product">$110</p>
              <p className="list-start">
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
              </p>
              {/*các nút yêu cầu mua hàng và View Product sản phẩm*/}
              <p className="footer-product">
                <button type="button" className="btn btn-success btn-click-muahang">Shop Now</button>
                <button type="button" className="btn btn-info btn-click-xem-chi-tiet">View Product
                </button>
              </p>
            </div>
            {/*kết thúc phần thông tin của sản phầm*/}
          </div>
        </div>
        <div className="col-xs-12 col-sm-4 product" style={{padding: 0, height: '405px'}} id="pro2">
          <div className="sanpham">
            {/*phần đầu chứa hình ảnh của sản phẩm*/}
            <div className="header-image-product" style={{clear: 'both', height: '200px'}}>
              <div className="left" style={{width: '20%', height: '200px', float: 'left', paddingLeft: '10px'}}>
                <div className="box box1" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/puma/puma5.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
                <div className="box box2" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/puma/puma6.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
                <div className="box box3" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                  <img src="images/puma/puma7.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                </div>
              </div>
              <div className="right" style={{width: '80%', float: 'left'}}>
                <img src="images/puma/puma8.jpg" className="img-responsive img-change" />
              </div>
            </div>
            {/*kết thúc phần đầu của sản phẩm*/}
            {/*phần thông tin của sản phẩm*/}
            <br />
            <br />
            <div className="info-product" align="center">
              <p className="name-product" style={{fontWeight: 'bold'}}>Converse Old Skool Platform - Women
                Shoes
              </p>
              <p className="price-product">$110</p>
              <p className="list-start">
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
                <span style={{marginRight: '3px', fontSize: '22px'}} className="glyphicon glyphicon-star-empty" />
              </p>
              {/*các nút yêu cầu mua hàng và View Product sản phẩm*/}
              <p className="footer-product">
                <button type="button" className="btn btn-success btn-click-muahang">Shop Now</button>
                <button type="button" className="btn btn-info btn-click-xem-chi-tiet">View Product
                </button>
              </p>
            </div>
            {/*kết thúc phần thông tin của sản phầm*/}
          </div>
        </div>
      </div>
      {/*kết thúc phần danh sách các sản phầm*/}
    </>
  );
}
export default JordanProduct;