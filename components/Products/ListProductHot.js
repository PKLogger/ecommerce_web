
import { React, useState, useEffect } from 'react';

function ListProductHot() {
  return (
    <>
      <div className="list-product-hot">
        <div className="container">
          <div className="row" style={{marginTop: '15px', borderTop: '1px solid', borderBottom: '1px solid', padding: '10px 0px 10px 0px'}}>
            <h2>You May Also Like</h2>
            <div className="col-xs-12 col-sm-3">
              <div className="sanpham" id="sanpham1">
                {/*phần đầu chứa hình ảnh của sản phẩm*/}
                <div className="header-image-product" style={{clear: 'both', height: '220px'}}>
                  <div className="left" style={{width: '20%', height: '200px', float: 'left', paddingLeft: '10px'}}>
                    <div className="box box1" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                      <img src="images/sanpham/sanpham2.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                    </div>
                    <div className="box box2" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                      <img src="images/sanpham/sanpham4.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                    </div>
                    <div className="box box3" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                      <img src="images/sanpham/sanpham3.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                    </div>
                  </div>
                  <div classa="right" style={{width: '80%', float: 'left'}}>
                    <img src="images/sanpham/sanpham1.jpg" className="img-responsive img-change" />
                  </div>
                </div>
                {/*kết thúc phần đầu của sản phẩm*/}
                {/*phần thông tin của sản phẩm*/}
                <br />
                <br />
                <div className="info-product" align="center">
                  <p className="name-product" style={{fontWeight: 'bold'}}>Vans Old Skool Platform - Men
                    Shoes
                  </p>
                  <p className="price-product" style={{color: '#121212', fontWeight: 'bold', fontSize: '17px'}}>$110</p>
                  <div id="rating2" style={{marginBottom: '20px'}}>
                    <div className="rating-stars text-center">
                      <ul id="stars">
                        <li className="star" title="Poor" data-value={1}>
                          <i className="fa fa-star fa-fw" />
                        </li>
                        <li className="star" title="Fair" data-value={2}>
                          <i className="fa fa-star fa-fw" />
                        </li>
                        <li className="star" title="Good" data-value={3}>
                          <i className="fa fa-star fa-fw" />
                        </li>
                        <li className="star" title="Excellent" data-value={4}>
                          <i className="fa fa-star fa-fw" />
                        </li>
                        <li className="star" title="WOW!!!" data-value={5}>
                          <i className="fa fa-star fa-fw" />
                        </li>
                      </ul>
                    </div>
                  </div>
                  {/*các nút yêu cầu mua hàng và xem chi tiết sản phẩm*/}
                </div>
                {/*kết thúc phần thông tin của sản phầm*/}
              </div>
            </div>
            <div className="col-xs-12 col-sm-3" style={{padding: 0}}>
              <div className="sanpham" id="sanpham2">
                {/*phần đầu chứa hình ảnh của sản phẩm*/}
                <div className="header-image-product" style={{clear: 'both', height: '220px'}}>
                  <div className="left" style={{width: '20%', height: '200px', float: 'left', paddingLeft: '10px'}}>
                    <div className="box box1" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                      <img src="images/giaynu/giaynu1.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                    </div>
                    <div className="box box2" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                      <img src="images/giaynu/giaynu3.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                    </div>
                    <div className="box box3" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                      <img src="images/giaynu/giaynu4.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                    </div>
                  </div>
                  <div classa="right" style={{width: '80%', float: 'left'}}>
                    <img src="images/giaynu/giaynu2.jpg" className="img-responsive img-change" />
                  </div>
                </div>
                {/*kết thúc phần đầu của sản phẩm*/}
                {/*phần thông tin của sản phẩm*/}
                <br />
                <br />
                <div className="info-product" align="center">
                  <p className="name-product" style={{fontWeight: 'bold'}}>Vans Old Skool Platform - Women
                    Shoes
                  </p>
                  <p className="price-product" style={{color: '#121212', fontWeight: 'bold', fontSize: '17px'}}>$110</p>
                  <div id="rating3" style={{marginBottom: '20px'}}>
                    <div className="rating-stars text-center">
                      <ul id="stars">
                        <li className="star" title="Poor" data-value={1}>
                          <i className="fa fa-star fa-fw" />
                        </li>
                        <li className="star" title="Fair" data-value={2}>
                          <i className="fa fa-star fa-fw" />
                        </li>
                        <li className="star" title="Good" data-value={3}>
                          <i className="fa fa-star fa-fw" />
                        </li>
                        <li className="star" title="Excellent" data-value={4}>
                          <i className="fa fa-star fa-fw" />
                        </li>
                        <li className="star" title="WOW!!!" data-value={5}>
                          <i className="fa fa-star fa-fw" />
                        </li>
                      </ul>
                    </div>
                  </div>
                  {/*các nút yêu cầu mua hàng và xem chi tiết sản phẩm*/}
                </div>
                {/*kết thúc phần thông tin của sản phầm*/}
              </div>
            </div>
            <div className="col-xs-12 col-sm-3" style={{padding: 0}}>
              <div className="sanpham" id="sanpham3">
                {/*phần đầu chứa hình ảnh của sản phẩm*/}
                <div className="header-image-product" style={{clear: 'both', height: '220px'}}>
                  <div className="left" style={{width: '20%', height: '200px', float: 'left', paddingLeft: '10px'}}>
                    <div className="box box1" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                      <img src="images/kid/kid1.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                    </div>
                    <div className="box box2" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                      <img src="images/kid/kid2.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                    </div>
                    <div className="box box3" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                      <img src="images/kid/kid3.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                    </div>
                  </div>
                  <div classa="right" style={{width: '80%', float: 'left'}}>
                    <img src="images/kid/kid4.jpg" className="img-responsive img-change" />
                  </div>
                </div>
                {/*kết thúc phần đầu của sản phẩm*/}
                {/*phần thông tin của sản phẩm*/}
                <br />
                <br />
                <div className="info-product" align="center">
                  <p className="name-product" style={{fontWeight: 'bold'}}>Vans Old Skool Platform - Kid
                    Shoes
                  </p>
                  <p className="price-product" style={{color: '#121212', fontWeight: 'bold', fontSize: '17px'}}>$110</p>
                  <div id="rating4" style={{marginBottom: '20px'}}>
                    <div className="rating-stars text-center">
                      <ul id="stars">
                        <li className="star" title="Poor" data-value={1}>
                          <i className="fa fa-star fa-fw" />
                        </li>
                        <li className="star" title="Fair" data-value={2}>
                          <i className="fa fa-star fa-fw" />
                        </li>
                        <li className="star" title="Good" data-value={3}>
                          <i className="fa fa-star fa-fw" />
                        </li>
                        <li className="star" title="Excellen" data-value={4}>
                          <i className="fa fa-star fa-fw" />
                        </li>
                        <li className="star" title="WOW" data-value={5}>
                          <i className="fa fa-star fa-fw" />
                        </li>
                      </ul>
                    </div>
                  </div>
                  {/*các nút yêu cầu mua hàng và xem chi tiết sản phẩm*/}
                </div>
                {/*kết thúc phần thông tin của sản phầm*/}
              </div>
            </div>
            <div className="col-xs-12 col-sm-3" style={{padding: 0}}>
              <div className="sanpham" id="sanpham4">
                {/*phần đầu chứa hình ảnh của sản phẩm*/}
                <div className="header-image-product" style={{clear: 'both', height: '220px'}}>
                  <div className="left" style={{width: '20%', height: '200px', float: 'left', paddingLeft: '10px'}}>
                    <div className="box box1" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                      <img src="images/sanpham/sanpham5.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                    </div>
                    <div className="box box2" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                      <img src="images/sanpham/sanpham7.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                    </div>
                    <div className="box box3" style={{width: '40px', height: '42px', marginBottom: '10px', clear: 'both'}}>
                      <img src="images/sanpham/sanpham2.jpg" style={{display: 'block', width: '40px', height: '40px'}} className="img-responsive" />
                    </div>
                  </div>
                  <div classa="right" style={{width: '80%', float: 'left'}}>
                    <img src="images/sanpham/sanpham6.jpg" className="img-responsive img-change" />
                  </div>
                </div>
                {/*kết thúc phần đầu của sản phẩm*/}
                {/*phần thông tin của sản phẩm*/}
                <br />
                <br />
                <div className="info-product" align="center">
                  <p className="name-product" style={{fontWeight: 'bold'}}>Vans Old Skool Platform - Men
                    Shoes
                  </p>
                  <p className="price-product" style={{color: '#121212', fontWeight: 'bold', fontSize: '17px'}}>$110</p>
                  <div id="rating5" style={{marginBottom: '20px'}}>
                    <div className="rating-stars text-center">
                      <ul id="stars">
                        <li className="star" title="Poor" data-value={1}>
                          <i className="fa fa-star fa-fw" />
                        </li>
                        <li className="star" title="Fair&quot;" data-value={2}>
                          <i className="fa fa-star fa-fw" />
                        </li>
                        <li className="star" title="Good" data-value={3}>
                          <i className="fa fa-star fa-fw" />
                        </li>
                        <li className="star" title="Excellen" data-value={4}>
                          <i className="fa fa-star fa-fw" />
                        </li>
                        <li className="star" title="WOW" data-value={5}>
                          <i className="fa fa-star fa-fw" />
                        </li>
                      </ul>
                    </div>
                  </div>
                  {/*các nút yêu cầu mua hàng và xem chi tiết sản phẩm*/}
                </div>
                {/*kết thúc phần thông tin của sản phầm*/}
              </div>
            </div>
          </div>
          <div className="modal fade" id="cart-model" tabIndex={-1} role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span><span className="sr-only">Close</span>
                  </button>
                  <h4 className="modal-title" id="myModalLabel">
                    DETAILS SHOP
                  </h4>
                </div>
                <div className="modal-body">
                  <div className="top-model" style={{overflow: 'hidden', borderBottom: '1px solid lightgray'}}>
                    <div className="top-left-model" style={{width: '40%', float: 'left'}}>
                      <img src="images/sanpham/sanpham6.jpg" className="img-model" style={{display: 'block', width: '200px', height: '200px'}} />
                    </div>
                    <div className="top-right-model" style={{width: '60%', float: 'left', marginLeft: '-20px'}}>
                      <h4>
                        Nike Air Force 1 Ultraforce Hi - Men Shoes<br />
                      </h4>
                      <p>
                        Amount:1 <br />
                        Color: White<br />
                        Size :39
                      </p>
                    </div>
                  </div>
                  {/*kết thúc phần đầu*/}
                  <div className="body-model" style={{marginTop: '10px', paddingBottom: '15px', borderBottom: '1px solid lightgray'}}>
                    <span><strong>Transport fee:</strong> <span>FREE</span></span> <br />
                    <span><strong>
                        TOTAL AMOUNT:</strong><span>  $110</span></span>
                    <br />
                    <br />
                    <button className="btn btn-success btn-click-view-cart btn-block">
                      VIEW CART</button>
                  </div>
                  {/*kết thúc phần thân model*/}
                </div>
                <div className="modal-footer">
                  <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>        
        </div>
      </div>
    </>
  );
}
export default ListProductHot;