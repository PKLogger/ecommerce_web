
import { React, useState, useEffect } from 'react';

function ProductDetails() {
  return (
    <>
      <div className="product-detail">
        <div className="container">
          <div className="row">
            <p style={{height: '40px', backgroundColor: '#d7d7d7', lineHeight: '40px', color: '#121212', textIndent: '1%'}}>HOME &gt; NIKE &gt; MENSHOES
            </p>
          </div>
        </div>
        <div className="container">
          <div className="row">
            {/*phần bên trái hiển thị  hình ảnh sản phẩm*/}
            <div className="col-xs-12 col-sm-7" style={{padding: 0}}>
              {/*nơi chứa hình ảnh chính ( slide trượt*/}
              <div className="img-top" style={{width: '100%', overflow: 'hidden'}}>
                <img src="images/anhchitiet/anhchitiet3.1.png" alt="..." className="img-responsive img-top-change" style={{display: 'block'}} />
              </div>
              {/*kết thúc  phần trượt sản phẩm*/}
              {/*phần chứa ảnh chi tiết của prodcut*/}
              <div className="img-detail-part" style={{marginTop: '5%', borderTop: '1px solid lightgray', padding: '10px 0px 10px 0px', clear: 'both'}}>
                <div className="box-detail box-detail1" style={{width: '80px', height: '80px', display: 'inline-block', marginRight: '15px'}}>
                  <img src="images/anhchitiet/anhchitiet31.png" style={{width: '70px', height: '70px', display: 'block'}} className="image-detail img-responsive" />
                </div>
                <div className="box-detail box-detail2" style={{width: '80px', height: '80px', display: 'inline-block', marginRight: '15px'}}>
                  <img src="images/anhchitiet/anhchitiet32.png" style={{width: '70px', height: '70px', display: 'block'}} className="image-detail img-responsive" />
                </div>
                <div className="box-detail box-detail3" style={{width: '80px', height: '80px', display: 'inline-block', marginRight: '15px'}}>
                  <img src="images/anhchitiet/anhchitiet33.png" style={{width: '70px', height: '70px', display: 'block'}} className="image-detail img-responsive" />
                </div>
                <div className="box-detail box-detail4" style={{width: '80px', height: '80px', display: 'inline-block', marginRight: '15px'}}>
                  <img src="images/anhchitiet/anhchitiet31.png" style={{width: '70px', height: '70px', display: 'block'}} className="image-detail img-responsive" />
                </div>
              </div>
              {/*các sản phẩm cùng loại*/}
              <div className="other-product" style={{marginTop: '50px', paddingBottom: '20px'}}>
                <p className="title" style={{fontSize: '22px', fontWeight: 'bold'}}>
                  OTHER COLORS
                </p>
                <div className="list-other-product" style={{clear: 'both'}}>
                  <div className="other other1" style={{width: '180px', height: '180px', float: 'left', marginRight: '70px', cursor: 'pointer'}}>
                    <img src="images/sanpham/sanpham8.jpg" style={{display: 'inline-block', width: '180px', height: '180px'}} />
                  </div>
                </div>
                <div className="list-other-product" style={{}}>
                  <div className="other other2" style={{width: '180px', height: '180px', float: 'left', cursor: 'pointer'}}>
                    <img src="images/sanpham/sanpham9.jpg" style={{display: 'inline-block', width: '180px', height: '180px'}} />
                  </div>
                </div>
              </div>
            </div>
            {/*kết thúc phần hiển  thị hình ảnh*/}
            {/*phần bên phải hiển thi thông tin sản phẩm*/}
            <div className="col-xs-12 col-sm-5" style={{}}>
              {/* phần chứa logo của sản ;phẩm */}
              <div className="logo" style={{height: '100px', width: '100px'}}>
                <img src="images/logo/logo-nike.jpg" style={{display: 'block', width: '100px', height: '100px'}} />
              </div>
              <div className="info-product" style={{}}>
                <p className="name-product" style={{fontSize: '25px', fontWeight: 700}}>Nike Air Force 1 Ultraforce Hi - Men Shoes</p>
                <p className="price" style={{fontSize: '28px'}}>$110</p>
                <h2 style={{fontSize: '18px', fontWeight: 700}}>
                  Product details for Nike Air Force 1 Ultraforce Hi - Men Shoes
                </h2>
                <p>
                  Color: Dk Team Red-Summit White-Dk Team Red<br />
                  Color: Dk Team Red-Summit White-Dk Team Red<br />
                  Material: Suede<br />
                  Product Code: 314107174204
                </p>
                <div className="binhchon">
                  <p>
                    Product Reviews <i>(Click on the star)</i>
                  </p>
                  <div id="rating1">
                    <label>Rating</label>
                  </div>
                  <button type="submit" className="btn btn-primary btn-block">Rating
                    <span className="review-text" style={{display: 'none'}}><span id="starCount" /> Star</span>
                  </button>
                </div>
                {/* khu vực lựa chọn kích thước sản phẩm */}
                <div className="choose-size-product" style={{marginTop: '10px'}}>
                  <p style={{fontWeight: 'bold'}}>Size Selection</p>
                  <div className="btn-group">
                    <button type="button" className="btn btn-default btn-lg btn-s">S</button>
                    <button type="button" className="btn btn-success btn-lg btn-m">M</button>
                    <button type="button" className="btn btn-info btn-lg btn-l">L</button>
                    <button type="button" className="btn btn-primary btn-lg btn-xl">XL</button>
                    <button type="button" className="btn btn-danger btn-lg btn-xxl">XXL</button>
                  </div>
                  <div className="box-size" style={{marginTop: '10px'}}>
                    <div className="box-size-pro box1" style={{width: '45px', height: '45px', display: 'inline-block', border: '1px solid gray', borderRadius: '4px', textAlign: 'center', lineHeight: '42px', marginRight: '15px', marginBottom: '10px'}}>
                      45
                    </div>
                    <div className="box-size-pro box2" style={{width: '45px', height: '45px', display: 'inline-block', border: '1px solid gray', borderRadius: '4px', textAlign: 'center', lineHeight: '42px', marginRight: '15px', marginBottom: '10px'}}>
                      46
                    </div>
                    <div className="box-size-pro box3" style={{width: '45px', height: '45px', display: 'inline-block', border: '1px solid gray', borderRadius: '4px', textAlign: 'center', lineHeight: '42px', marginRight: '15px', marginBottom: '10px'}}>
                      47
                    </div>
                    <div className="box-size-pro box4" style={{width: '45px', height: '45px', display: 'inline-block', border: '1px solid gray', borderRadius: '4px', textAlign: 'center', lineHeight: '42px', marginRight: '15px', marginBottom: '10px'}}>
                      48
                    </div>
                    <div className="box-size-pro box5" style={{width: '45px', height: '45px', display: 'inline-block', border: '1px solid gray', borderRadius: '4px', textAlign: 'center', lineHeight: '42px', marginRight: '15px', marginBottom: '10px'}}>
                      49
                    </div>
                    <div className="box-size-pro box6" style={{width: '45px', height: '45px', display: 'inline-block', border: '1px solid gray', borderRadius: '4px', textAlign: 'center', lineHeight: '42px', marginRight: '15px', marginBottom: '10px'}}>
                      50
                    </div>
                    <div className="box-size-pro box7" style={{width: '45px', height: '45px', display: 'inline-block', border: '1px solid gray', borderRadius: '4px', textAlign: 'center', lineHeight: '42px', marginRight: '15px', marginBottom: '10px'}}>
                      51
                    </div>
                    <div className="box-size-pro box9" style={{width: '45px', height: '45px', display: 'inline-block', border: '1px solid gray', borderRadius: '4px', textAlign: 'center', lineHeight: '42px', marginRight: '15px', marginBottom: '10px'}}>
                      52
                    </div>
                    <div className="box-size-pro box7" style={{width: '45px', height: '45px', display: 'inline-block', border: '1px solid gray', borderRadius: '4px', textAlign: 'center', lineHeight: '42px', marginRight: '15px', marginBottom: '10px'}}>
                      51
                    </div>
                    <div className="box-size-pro box9" style={{width: '45px', height: '45px', display: 'inline-block', border: '1px solid gray', borderRadius: '4px', textAlign: 'center', lineHeight: '42px', marginRight: '15px', marginBottom: '10px'}}>
                      52
                    </div>
                  </div>
                  <div className="add-to-cart">
                    <button className="btn btn-success btn-lg btn-block btn-click-add-to-cart disabled">
                      ADD TO CART
                    </button>
                  </div>
                </div>
              </div>
            </div>
            {/*kết thúc phần phía bên phải*/}
          </div>
        </div>
      </div>
    </>
  );
}
export default ProductDetails;