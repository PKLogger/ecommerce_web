export const loadLanguage = () => {
    try {
        const serializedState = localStorage.getItem('LANGUAGE')
        if (serializedState === null) {
            return undefined
        }
        return JSON.parse(serializedState)
    }
    catch (err) {
        return undefined
    }
}

export const saveLanguage = (state) => {
    try {
        const serializedState = JSON.stringify(state)
        localStorage.setItem('LANGUAGE', serializedState)
    }
    catch (err) {
        // ignore
    }
}