import { createStore } from 'redux';
import rootReducer from './reducers';
import { saveLanguage, loadLanguage } from './localStorage'

const store = createStore(rootReducer, loadLanguage());

store.subscribe(() => {
    saveLanguage({
        changeLanguage: store.getState().changeLanguage
    })
})

export default store;