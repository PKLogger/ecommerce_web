import * as types from "../constants/ActionTypes";

var data = { loading: false };
var initailState = data ? data : {};

const setLoading = (state = initailState, action) => {
  // var { accessToken, userInfo } = action;
  var newState = { ...state };
  switch (action.type) {
    case types.SET_LOADING:
      newState = action.data;
      // localStorage.setItem("LOADING", JSON.stringify(newState));
      return newState;
    default:
      return state;
  }
};

export default setLoading;
