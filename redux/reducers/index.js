import { combineReducers } from "redux";
import changeLanguage from "./changeLanguage";
import setLoading from "./setLoading";

const appReducer = combineReducers({
  changeLanguage,
  setLoading
});

export default appReducer;
