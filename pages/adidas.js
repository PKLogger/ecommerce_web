import Layout from './layout';
import { React, useState, useEffect } from 'react';
import FilterProduct from '../components/Products/FilterProduct';
import ListOfDetailsProduct from '../components/Products/ListOfDetailsProduct';
import AdidasProduct from '../components/Products/AdidasProduct';

function Adidas() {
  return (
    <>
      <Layout title="">
        <div className="body-list-product" style={{clear: 'both'}}>
          <div className="container">
            <div className="row">
              <div className="col-xs-12 col-sm-3" style={{border: '1px solid lightgray', padding: 0, clear: 'both'}}>
                <FilterProduct />
              </div>
              <div className="col-xs-12 col-sm-9" style={{paddingLeft: '5px', paddingRight: 0}}>
                <ListOfDetailsProduct />
                <AdidasProduct />
              </div>
            </div>
          </div>
        </div>
      </Layout>
    </>
  );
}
export default Adidas;