import Layout from './layout';
import { React, useState, useEffect } from 'react';
import ListBrand from '../components/Home/ListBrand';

function FlashSale() {
  return (
    <>
      <Layout title="">
        <div className="sale-slide">
          <div className="container">
            <div className="row">
              <img src="images/sale.png" alt="" />
            </div>
          </div>
        </div>
        <ListBrand />
      </Layout>
    </>
  );
}
export default FlashSale;