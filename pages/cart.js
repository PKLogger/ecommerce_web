import Layout from './layout';
import { React, useState, useEffect } from 'react';
import ListBrand from '../components/Home/ListBrand';

function Cart() {
  const [amount, setAmount] = useState(0);
  return (
    <>
      <Layout title="">
        {/* phần  chi tiết các sản phẩm trong giỏ hanngf*/}
        <div className="table-product">
          <div className="container">
            <div className="row">
              <div className="table-responsive">
                <table className="table table-hover table-bordered">
                  <thead>
                    <tr>
                      <th width="10%">ID</th>
                      <th width="25%">IMAGE</th>
                      <th width="20%">PRODUCT NAME</th>
                      <th width="15%">COLOR</th>
                      <th width="10%">SIZE</th>
                      <th width="10%">AMOUNT</th>
                      <th width="20%">SUM</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>
                        <img src="images/sanpham/sanpham6.jpg" style={{display: 'block', width: '200px', height: '200px'}} />
                      </td>
                      <td>
                        <h4>
                          Nike Air Force 1 Ultraforce Hi - Men Shoes<br />
                        </h4>
                        <p>ID: 314107173404075</p>
                      </td>
                      <td>white</td>
                      <td>39</td>
                      <td>
                        <div class="form-group">
                          <input type="number" class="form-control" id="so-luong" placeholder="0" />
                        </div>
                      </td>
                      <td className="sum-money">110</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="row">
              <div className="left">
                <div className="col-xs-12 col-sm-3 ">
                  <button type="button" className="btn btn-success btn-click-go-to-home btn-block">Continue shopping</button>
                </div>
              </div>
              <div className="right" align="right">
                <div className="col-xs-12 col-sm-3 col-sm-offset-6">
                  <button type="button" className="btn btn-danger btn-click-checkout btn-block">Check Out</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <ListBrand />
      </Layout>
    </>
  );
}
export default Cart;