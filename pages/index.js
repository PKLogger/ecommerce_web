import Layout from './layout';
import { React, useState, useEffect } from 'react';
import HomeSlider from '../components/Home/HomeSlider';
import ListBrand from '../components/Home/ListBrand';
import ListProduct from '../components/Home/ListProduct';

function Home() {
  return (
    <Layout title="">
      <HomeSlider/>
      <ListBrand/>
      <ListProduct />
    </Layout>
  );
}
export default Home;