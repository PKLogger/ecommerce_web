import Layout from './layout';
import { React, useState, useEffect } from 'react';
import SignIn from '../components/Login/SignIn';
import SignUp from '../components/Login/SignUp';

function Login() {
  return (
    <Layout title="">
      <div className="container" style={{borderTop: '2px solid', paddingTop: '5px', borderBottom: '2px solid', paddingBottom: '5px'}}>
        <div className="row">
          <SignIn />
          <SignUp />
        </div>
      </div>
    </Layout>
  );
}
export default Login;