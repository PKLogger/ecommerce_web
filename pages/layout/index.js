import React from 'react';
import Header from './Header';
import Footer from './Footer';
import { connect } from "react-redux";
import Head from 'next/head';

function Layout({
    children,
    title = 'Ecommerce web',
}) {
    
    return (
        <>
            <Head>
                <title>{title}</title>
                <link rel="icon" href="https://dcv.vn/wp-content/uploads/2021/01/logo-dcv-2021-1.png" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                {/* <meta property="og:title" content={title} />
                <meta property="og:description" content={title} />
                <meta property="og:image" content={image} /> */}
                <meta name="keywords" content="" />
                <meta name="description" content="" />
                <link rel="canonical" href="https://dcv.vn/" />
                <meta property="og:locale" content="vi_VN" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content="Trang chủ - Ecommerce web" />
                <meta property="og:description" content="Trang chủ Ecommerce web" />
                <meta property="og:url" content="https://dcv.vn/" />
                <meta property="og:site_name" content="Công ty cổ phần Truyền số liệu Việt Nam" />
                <meta property="fb:app_id" content="405144220230880" />
                <meta property="og:image" content="http://dcv.vn/wp-content/uploads/2019/02/51773408_10157174295652764_513871757266911232_n.jpg" />
                <meta name="twitter:card" content="summary" />
                <meta name="twitter:description" content="Trang chủ DCV website" />
                <meta name="twitter:title" content="Trang chủ - Công ty cổ phần Truyền số liệu Việt Nam" />
                <meta name="twitter:image" content="http://dcv.vn/wp-content/uploads/2019/02/51773408_10157174295652764_513871757266911232_n.jpg" />
                <link rel='dns-prefetch' href='//s.w.org' />
                <link rel="alternate" type="application/rss+xml" title="Dòng thông tin Công ty cổ phần Truyền số liệu Việt Nam &raquo;" href="https://dcv.vn/feed/" />
                <link rel="alternate" type="application/rss+xml" title="Dòng phản hồi Công ty cổ phần Truyền số liệu Việt Nam &raquo;" href="https://dcv.vn/comments/feed/" />
                
                {/* Fav Icon ========================================= */}
                <link id="favicon" rel="Icon" type="image/png" href="images/favicon.ico"></link>

                {/* CSS FILES ========================================= */}
                <link rel='stylesheet' id='bootstrap-min-css'  href='/css/bootstrap.min.css' type='text/css' media='all' />
                <link rel='stylesheet' id='font-awesome-min-css'  href='css/font-awesome.min.css' type='text/css' media='all' />
                <link rel='stylesheet' id='style-css'  href='css/style.css' type='text/css' media='all' />
                <link rel='stylesheet' id='style-list-product-css'  href='css/styleListproduct.css' type='text/css' media='all' />
                <link rel='stylesheet' id='style-product-detail-css'  href='css/styleproductDetail.css' type='text/css' media='all' />
                <link rel='stylesheet' id='style-rating-css'  href='css/styleRating.css' type='text/css' media='all' />

                {/* JAVASCRIPT FILES ========================================= */}
                
                <script type='text/javascript' src="js/library/jquery-3.2.1.min.js"></script>
                <script type='text/javascript' src="js/library/bootstrap.min.js"></script>
                <script type='text/javascript' src="js/others/popper.min.js"></script>
                <script type='text/javascript' src="js/config/event2.js"></script>
                <script type='text/javascript' src="js/config/menuEvent.js"></script>
                <script type='text/javascript' src="js/function/cartPage.js"></script>
                <script type='text/javascript' src="js/function/rating.js"></script>
                <script type='text/javascript' src="js/function/detail.js"></script>
                <script type='text/javascript' src="js/function/productDetails3.js"></script>
                
                {/* <script type='text/javascript' src="js/function.js"></script> */}
            </Head>
            <Header />
            <div id="c-scroll-smooth-id" className="c-scroll-smooth-id">
                {children}
                <Footer />
            </div>
        </>
    );
}

const mapStateToProps = state => {
    return {
        setloading: state.setLoading
    };
};
export default connect(mapStateToProps, null)(Layout);