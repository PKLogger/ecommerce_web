import React, { useState } from 'react';
import Link from 'next/link';
import { Container, Nav, Navbar } from 'react-bootstrap';

function Header(props) {
    return (
        <>
            {/* Header Top-bar */}
            <div className="top-bar">
                <div className="container">
                    <div className="row">
                        <div className="logo col-sm-6 clearfix">
                            <a href="/" className="logo"><img src="images/logo/logo-website.png" alt="logo" /></a>
                        </div>
                        <div className="col-sm-6 clearfix">
                            <div className="row" style={{marginTop: '2%', marginBottom: '2%'}}>
                                <Link href="/login"><span className="float-right" /*id="btn-login"*/>Login / Register</span></Link>
                            </div>
                            <div className="row">
                                <div className="col-md-8 col-xs-8" style={{paddingRight: 0, paddingLeft: 0}}>
                                    <div className="search clearfix">
                                        <input type="search" id="search" placeholder="Enter your search here" />
                                        <button className="icon"><i className="fa fa-search float-right" /></button>
                                    </div>
                                </div>
                                <div className="col-md-4 col-xs-4" style={{paddingRight: 0, paddingLeft: 0}}>
                                    <div className="cart clearfix">
                                        <span className="cart-text">0 ITEMS</span>
                                        <span className="float-right"><a href="#" className="fa fa-shopping-cart" /></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* Header main-menu */}
            <div className="main-menu">
                <div className="container">
                    <nav className="navbar navbar-inverse">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                <span className="icon-bar" /><span className="icon-bar" /><span className="icon-bar" />                        
                            </button>
                            <a href="#" className="navbar-brand"><img src="images/logo/logo-website.png" alt="logo" /></a>
                        </div>
                        <div className="navbar-collapse collapse" id="myNavbar">
                            <ul className="nav navbar-nav">
                                <li className="dropdown" id="f">
                                    <a className="dropdown-toggle" data-toggle="dropdown" id="dropdownMenuLink" href="#">MAN 
                                        <i className="fa fa-angle-double-down" />
                                        <i className="fa fa-angle-double-up" />
                                    </a>
                                    
                                    <ul className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <li><Link href="/adidas" className="dropdown-item adidas">Adidas</Link></li>
                                        <li><Link href="/van" className="dropdown-item van">Van</Link></li>
                                        <li><Link href="/converse" className="dropdown-item converse">Converse</Link></li>
                                        <li><Link href="/nike" className="dropdown-item nike">Nike</Link></li>
                                        <li><Link href="/palladium" className="dropdown-item palladium">Palladium</Link></li>
                                        <li><Link href="/jordan" className="dropdown-item jordan">Jordan</Link></li>
                                    </ul>
                                    
                                </li>
                                <li className="dropdown">
                                    <a className="dropdown-toggle" data-toggle="dropdown" href="#">WOMAN <i className="fa fa-angle-double-down" /><i className="fa fa-angle-double-up" /></a>
                                    <ul className="dropdown-menu">
                                        <li><Link href="/adidas" className="adidas">Adidas</Link></li>
                                        <li><Link href="/van" className="van">Van</Link></li>
                                        <li><Link href="/converse" className="converse">Converse</Link></li>
                                        <li><Link href="/nike" className="nike">Nike</Link></li>
                                        <li><Link href="/palladium" className="palladium">Palladium</Link></li>
                                        <li><Link href="/jordan" className="jordan">Jordan</Link></li>
                                    </ul>
                                </li>
                                <li className="dropdown">
                                    <a className="dropdown-toggle" data-toggle="dropdown" href="#">KID <i className="fa fa-angle-double-down" /><i className="fa fa-angle-double-up" /></a>
                                    <ul className="dropdown-menu">
                                        <li><Link href="/adidas" className="adidas">Adidas</Link></li>
                                        <li><Link href="/van" className="van">Van</Link></li>
                                        <li><Link href="/converse" className="converse">Converse</Link></li>
                                        <li><Link href="/nike" className="nike">Nike</Link></li>
                                        <li><Link href="/palladium" className="palladium">Palladium</Link></li>
                                        <li><Link href="/jordan" className="jordan">Jordan</Link></li>
                                    </ul>
                                </li>
                                <li><Link href="/flash-sale" className="sale">FLASH SALE</Link></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </>
    );
}

export default Header;

