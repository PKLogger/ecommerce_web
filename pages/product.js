
import { React, useState, useEffect } from 'react';
import ListProductHot from '../components/Products/ListProductHot';
import ProductDetails from '../components/Products/ProductDetails';
import Layout from './layout';
function Product() {
  return (
    <>
      <Layout title="">
        <ProductDetails />
        <ListProductHot />
      </Layout>
    </>
  );
}
export default Product;