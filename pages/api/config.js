import getConfig from 'next/config';
const { publicRuntimeConfig } = getConfig();
const config = {
    // HOST: 'http://api-megaone.dcv.vn:3000'
    HOST: publicRuntimeConfig.API_URL || 'http://128.199.96.181:4018'
    // HOST: __DEV__ ? 'http://staging.mpoint.vn/' : 'http://prod.mpoint.vn/',
};
export default config;