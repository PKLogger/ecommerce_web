import request from './request';
let dataService = {
  createStudent: (params) => {
    let url = 'api/v1/student/create-student';
    return request.post(params, url);
  },
  getSurvey: (params) => {
    let url = 'api/v1/survey/find-survey';
    return request.get1(params, url);
  },
};

export default dataService;
