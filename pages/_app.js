// import '../styles/globals.css';
import '../styles/static/css/style.css';
// import "bootstrap/dist/css/bootstrap.min.css";
import "animate.css";
import "font-awesome/css/font-awesome.min.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
// import App from 'next/app';
import { Provider } from 'react-redux';
import React from 'react';
import store from '../redux/store';

const MyApp = ({ Component, pageProps }) => {
  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  );
};
export default MyApp
