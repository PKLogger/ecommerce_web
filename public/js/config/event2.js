//  xử lý việc đăng nhập đăng xuất : khi click vào  nút đăng nhập -> chuyển đến trang đăng nhập. đăng kí
$(document).ready(function () {
    $("#btn-login").click(function () {
        window.location  = "/login";
    });
    // khi click vào một trong các sản phẩm thì chuyển sang trang sản phẩm
    // khi clikc vào xem chi tiết tại trang chủ --> chuyển đến trang xem chi tiêtas
    $('.btn-add-cart').click(function (){
        window.location  = "/cart";
    });
    $(".btn-click-xem-chi-tiet").click(function () {
        window.location  = "/product";
    });
    $(".sale").click(function () {
        window.location  = "/flash-sale";
    });
    $(".new-arrival").click(function () {
        window.location  = "newarrivalPage.html";
    });
    $(".btn-guess-checkout").click(function () {
        window.location  = "/shippingpayment";
    });
    // click vào logo quay tro ve trang chu
    $(".logo").click(function (){
        window.location= '/';
    });
});


